The upstream changelog and Debian changelog are the same file; we use
dpkg-mergechangelogs to merge them together when upstream makes a new
release.

This confuses dpkg-genchanges: since our X-1 changelog entry follows
upstream's X changelog entry, dpkg-genchanges thinks that this is a
revision and we don't need to include the upstream tarball in the
upload.

This also confuses dgit: it will pass -v to dpkg-genchanges such that
upstream's changelog entry is included in the .changes file.

To deal with this, pass -sa to dpkg-buildpackage.  If using dgit, the
magic incantation is:

    dgit --ch:-sa -v_ [s]build

 -- Sean Whitton <spwhitton@spwhitton.name>, Mon,  5 Sep 2016 16:59:32 -0700
